import * as knex from 'knex';
import { IUserColumns } from '../dto/user.dto';
import axios, { AxiosInstance, AxiosResponse } from 'axios';

export class UserModel {

  http: AxiosInstance;

  constructor() {
    this.http = axios.create()
  }

  getUser(db: knex) {
    return db('users').select('user_id', 'username', 'first_name', 'last_name')
  }

  info(db: knex, userId: any) {
    return db('users').where('user_id', userId)
  }

  create(db: knex, user: any) {
    return db('users').insert(user, 'user_id')
  }

  update(db: knex, userId: any, user: IUserColumns) {
    return db('users')
      .where('user_id', userId)
      .update(user)
  }

  login(db: knex, username: any, password: any) {
    return db('users')
      .select('user_id', 'username', 'first_name', 'last_name')
      .where('username', username)
      .where('password', password)
  }

  async getOtherServices(): Promise<AxiosResponse<any>> {
    return this.http.get(`http://xxx.xxx.xxx`, {
      headers: {
        'Authorization': 'Basic xxxxx'
      },
    })
  }

}