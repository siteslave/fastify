import app from './app'
import { join } from 'path'

require('dotenv').config({ path: join(__dirname, '../config.conf') })

// listening configuration
const port = +process.env.PORT || 3000
const host = process.env.HTTP_BINDING || '0.0.0.0'

const start = async () => {
    try {
        await app.listen(port, host);
        const info: any = app.server.address()
        //app.log.info(`Server listening on http://${info.address}:${info.port}`)
    } catch (err) {
        app.log.error(err)
        process.exit(1)
    }
}

start()