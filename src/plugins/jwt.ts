import * as HttpStatus from 'http-status-codes'
import { FastifyRequest, FastifyReply } from 'fastify'
var fastifyPlugin = require('fastify-plugin')

async function fastifyJwt(fastify: any, opts: any, next: any) {
  fastify.register(require("fastify-jwt"), {
    secret: opts.secret
  })

  try {
    fastify.decorate("authenticate", async function (req: FastifyRequest, reply: FastifyReply) {

      let token = null
      const body: any = req.body
      const query: any = req.query

      if (req.headers.authorization && req.headers.authorization.split(' ')[0].toLowerCase() === 'bearer') {
        token = req.headers.authorization.split(' ')[1]
      } else if (query && query.token) {
        token = query.token || null
      } else if (body && body.token) {
        token = body.token || null
      } else {
        token = null
      }

      if (token) {
        try {
          await req.jwtVerify(token)
          next()
        } catch (err) {
          reply.code(HttpStatus.UNAUTHORIZED).send({
            code: HttpStatus.UNAUTHORIZED,
            message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)
          });
        }
      } else {
        reply.code(HttpStatus.UNAUTHORIZED).send({
          code: HttpStatus.UNAUTHORIZED,
          message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)
        })
      }

    })

  } catch (err) {
    next(err)
  }
}

module.exports = fastifyPlugin(fastifyJwt, '>=0.30.0')
