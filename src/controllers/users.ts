import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'

import { UserModel } from '../models/user'

import * as knex from 'knex'
import * as crypto from 'crypto'

import createError = require('http-errors')
import userSchema from '../schema/user.schema'

export default async function users(fastify: FastifyInstance) {

  const db: knex = fastify.db
  const userModel = new UserModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'User router!' })
  })

  fastify.get('/list', {
    preHandler: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const rs: any = await userModel.getUser(db)
      reply.send(rs)
    } catch (error) {
      request.log.error(error.message)
      reply.send(createError(500, error.message))
    }
  })

  fastify.post('/register', {
    preHandler: [fastify.authenticate],
    schema: userSchema
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body
    const user: any = {}
    user.username = body.username;
    user.password = crypto.createHash('md5').update(body.password).digest('hex')
    user.first_name = body.firstName
    user.last_name = body.lastName

    try {
      const rs: any = await userModel.create(db, user)
      reply.send({ userId: rs[0] })
    } catch (error) {
      request.log.error(error.message)
      reply.send(createError(500, error.message))
    }
  })

}
