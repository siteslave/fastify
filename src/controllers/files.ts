import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { v4 as uuidv4 } from 'uuid'

import * as fse from 'fs-extra'
import * as path from 'path'
import * as fs from 'fs'
import util = require('util')

const { pipeline } = require('stream')

const pump = util.promisify(pipeline)

export default async function files(fastify: FastifyInstance) {

  const uploadDir = process.env.UPLOAD_DIR || './uploads'

  fse.ensureDirSync(uploadDir)

  fastify.post('/', {
    preHandler: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = await request.body
    const id = body.id.value
    const file = body.file

    const _ext = path.extname(file.filename)
    const newFileName = uuidv4() + _ext

    const filePath = path.join(uploadDir, newFileName)

    try {
      await pump(file.file, fs.createWriteStream(filePath))
      reply.send({ ok: true, fileName: file.filename })
    } catch (error) {
      reply.code(500).send({ statusCode: 500, error: error.message })
    }
  })

  fastify.get('/:fileName', (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params
    const fileName: any = params.fileName

    const filePath = path.join(uploadDir, fileName)
    const mimeType = 'image/jpeg'

    try {
      if (fs.existsSync(filePath)) {
        reply.type(mimeType)
        const fileData = fs.readFileSync(filePath)
        reply.send(fileData)
      } else {
        reply.code(400).send({
          statusCode: 404,
          error: 'File not found'
        })
      }
    } catch (error) {
      reply.code(500).send({ statusCode: 500, error: error.message })
    }
  })

}
