import * as fastify from 'fastify'

// import multer from 'fastify-multer'
import router from "./router"

import { join } from 'path'

require('dotenv').config({ path: join(__dirname, '../config.conf') })

const app: fastify.FastifyInstance = fastify.fastify({
  logger: { level: 'info' }
})

app.register(require('fastify-formbody'))
app.register(require('fastify-cors'), {})
app.register(require('fastify-no-icon'))
app.register(require('fastify-multipart'), {
  attachFieldsToBody: true,
  limits: {
    fieldNameSize: 100, // Max field name size in bytes
    fieldSize: 1000000, // Max field value size in bytes
    fields: 10,         // Max number of non-file fields
    fileSize: 100,      // For multipart forms, the max file size
    files: 1,           // Max number of file fields
    headerPairs: 2000   // Max number of header key=>value pairs
  }
})

app.register(require('fastify-static'), {
  root: join(__dirname, '../public'),
  prefix: '/public/'
})

app.register(require('point-of-view'), {
  engine: {
    ejs: require('ejs'),
    root: join(__dirname, 'views')
  },
  includeViewExtension: true
})

app.register(require('fastify-rate-limit'), {
  max: +process.env.MAX_CONNECTION_PER_MINUTE || 500,
  timeWindow: '1 minute',
  whitelist: ['127.0.0.1', '192.168.1.1']
  // global: false
})

app.register(require('./plugins/jwt'), {
  secret: process.env.SECRET_KEY || '1234567890xx'
})

// database connection
app.register(require('./plugins/db'), {
  connection: {
    client: 'mysql2',
    connection: {
      host: process.env.DB_HOST || 'localhost',
      user: process.env.DB_USER || 'root',
      port: +process.env.DB_PORT || 3306,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME || 'test',
    },
    // pool: {
    //   min: 0,
    //   max: 100
    // },
    debug: +process.env.DEBUG === 1 ? true : false,
  },
  connectionName: 'db'
})

// import routers
app.register(router)

export default app;
